// Initialize Firebase
const config = {
    apiKey: "AIzaSyDzYpZGrP4Fm47qXtNHYn6czCV-oFYe8Tc",
    authDomain: "crudvuejs-dd239.firebaseapp.com",
    databaseURL: "https://crudvuejs-dd239.firebaseio.com",
    projectId: "crudvuejs-dd239",
    storageBucket: "crudvuejs-dd239.appspot.com",
    messagingSenderId: "346578025451",
    appId: "1:346578025451:web:448c9b9f7e7508edc037ae",
    measurementId: "G-P40P219K6K"
};
firebase.initializeApp(config);
const db = firebase.database();

var appMoviles = new Vue({    
    el: "#appMoviles",   
    data:{     
      moviles:[],          
        marca:"",
        modelo:"",
        stock:"",
        total:0,       
    },    
    methods:{  
        //BOTONES        
        btnAlta:async function(){                    
            const {value: formValues} = await Swal.fire({
            title: 'NUEVO',
            html:
            '<div class="row"><label class="col-sm-3 col-form-label">Marca</label><div class="col-sm-7"><input id="marca" type="text" class="form-control"></div></div><div class="row"><label class="col-sm-3 col-form-label">Modelo</label><div class="col-sm-7"><input id="modelo" type="text" class="form-control"></div></div><div class="row"><label class="col-sm-3 col-form-label">Stock</label><div class="col-sm-7"><input  id="stock" type="number" min="0" class="form-control"></div></div>',              
            focusConfirm: false,
            showCancelButton: true,
            confirmButtonText: 'Guardar',          
            confirmButtonColor:'#1cc88a',          
            cancelButtonColor:'#3085d6',  
            preConfirm: () => {            
                return [
                this.marca = document.getElementById('marca').value,
                this.modelo = document.getElementById('modelo').value,
                this.stock = document.getElementById('stock').value                    
                ]
            }
            })        
            if(this.marca == "" || this.modelo == "" || this.stock == 0){
                    Swal.fire({
                    type: 'info',
                    title: 'Datos incompletos',                                    
                    }) 
            }       
            else{          
            this.altaMovil();          
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
                });
                Toast.fire({
                type: 'success',
                title: '¡Producto Agregado!'
                })                
            }
        },           
        btnEditar:async function(movil, key){                            
            await Swal.fire({
            title: 'EDITAR',
            html:
            '<div class="form-group"><div class="row"><label class="col-sm-3 col-form-label">Marca</label><div class="col-sm-7"><input id="marca" value="'+movil.marca+'" type="text" class="form-control"></div></div><div class="row"><label class="col-sm-3 col-form-label">Modelo</label><div class="col-sm-7"><input id="modelo" value="'+movil.modelo+'" type="text" class="form-control"></div></div><div class="row"><label class="col-sm-3 col-form-label">Stock</label><div class="col-sm-7"><input id="stock" value="'+movil.stock+'" type="number" min="0" class="form-control"></div></div></div>', 
            focusConfirm: false,
            showCancelButton: true,                         
            }).then((result) => {
            if (result.value) {                                             
                movil.marca = document.getElementById('marca').value,    
                movil.modelo = document.getElementById('modelo').value,
                movil.stock = document.getElementById('stock').value,                    
                
                this.editarMovil(movil,key);
                Swal.fire(
                '¡Actualizado!',
                'El registro ha sido actualizado.',
                'success'
                )                  
            }
            });
            
        },        
        btnBorrar:function(movil){        
            Swal.fire({
            title: '¿Está seguro de borrar el registro: '+movil.marca+": "+movil.modelo+ " ?",         
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor:'#d33',
            cancelButtonColor:'#3085d6',
            confirmButtonText: 'Borrar'
            }).then((result) => {
            if (result.value) {            
                this.borrarMovil(movil.key);             
                //y mostramos un msj sobre la eliminación  
                Swal.fire(
                '¡Eliminado!',
                'El registro ha sido borrado.',
                'success'
                )
            }
            })                
        },       
        
        //PROCEDIMIENTOS para el CRUD     
        listarMoviles:function(){
        db.ref('/productos').on('value', snapshot => this.Getmoviles(snapshot.val()));
        },
        // CARGAR
        Getmoviles(moviles) {
            this.moviles = [];
            for (let key in moviles) {
                this.moviles.push({
                    marca: moviles[key].marca,
                    modelo: moviles[key].modelo,
                    stock: moviles[key].stock,
                    key: key,
                });
            }
            this.moviles.reverse();
            console.log(this.moviles);
        },    
        //Procedimiento CREAR.
        altaMovil:function(){
            db.ref('productos').push({
              marca:this.marca,
              modelo:this.modelo,
              stock:this.stock
            }).then(
                response =>{
                this.listarMoviles();
            });        
            this.marca = "",
            this.modelo = "",
            this.stock = 0
        },               
        //Procedimiento EDITAR.
        editarMovil:function(movil,key){
          db.ref('/productos/' + key).update({
            marca:movil.marca,
            modelo:movil.modelo,
            stock:movil.stock
          });
          this.listarMoviles();                              
        },    
        //Procedimiento BORRAR.
        borrarMovil:function(key){
          db.ref('/productos/' + key).remove();
          this.listarMoviles();
        }             
    },      
    created: function(){            
    this.listarMoviles();            
    },    
    computed:{
        totalStock(){
            this.total = 0;
            for(movil of this.moviles){
                this.total = this.total + parseInt(movil.stock);
            }
            return this.total;   
        }
    }    
    });